#!/usr/bin/env python3.4
import argparse
import sys
import pathlib
import os
import tempfile
try:
    from fontTools.ttLib import TTFont, newTable
except ImportError:
    print('== Requires TTX/FontTools ==', file=sys.stderr)


class InputModifier(object):
    """
    An object for manipulating Input, takes a TTFont. Sorry this is a little hacky.
    """

    def __init__(self, f):
        self.f = f

    def changeLineHeight(self, lineHeight):
        """
        Takes a line height multiplier and changes the line height.
        """
        baseAsc = self.f['OS/2'].sTypoAscender
        baseDesc = self.f['OS/2'].sTypoDescender
        multiplier = float(lineHeight)
        self.f['hhea'].ascent = round(baseAsc * multiplier)
        self.f['hhea'].descent = round(baseDesc * multiplier)
        self.f['OS/2'].usWinAscent = round(baseAsc * multiplier)
        self.f['OS/2'].usWinDescent = round(baseDesc * multiplier) * -1

    def swap(self, swap):
        """
        Takes a dictionary of glyphs to swap and swaps 'em.
        """
        glyphNames = self.f.getGlyphNames()

        maps = {'a': {97: 'a',
                      229: 'aring',
                      228: 'adieresis',
                      1072: 'acyrillic',
                      225: 'aacute',
                      257: 'amacron',
                      224: 'agrave',
                      227: 'atilde',
                      226: 'acircumflex',
                      261: 'aogonek',
                      259: 'abreve'},
                'g': {289: 'gdotaccent',
                      287: 'gbreve',
                      285: 'gcircumflex',
                      291: 'gcommaaccent',
                      103: 'g'},
                'i': {105: 'i',
                      237: 'iacute',
                      303: 'iogonek',
                      236: 'igrave',
                      297: 'itilde',
                      238: 'icircumflex',
                      299: 'imacron',
                      307: 'ij',
                      301: 'ibreve',
                      1111: 'yicyrillic',
                      239: 'idieresis',
                      1110: 'icyrillic',
                      305: 'dotlessi'},
                'l': {108: 'l',
                      318: 'lcaron',
                      316: 'lcommaaccent',
                      314: 'lacute',
                      322: 'lslash',
                      320: 'ldot'},
                'zero': {48: 'zero'},
                'asterisk': {42: 'asterisk'},
                'braces': {123: 'braceleft', 125: 'braceright'}}

        swapMap = {}
        # get list of swap value
        for swap_requested, swap_value in swap.items():
            # for the current base character, get the list of related character in map
            for unicode_value, basic_gname in maps[swap_requested].items():
                # generate the name of the new charatcer to use
                newGname = basic_gname + '.salt_' + swap_value
                if swap_value in ['ds', 'dot', 'superscript', 'curly']:
                    newGname = basic_gname
                if newGname in glyphNames:
                    swapMap[unicode_value] = newGname
        # for each unicode select the character to use
        for table in self.f['cmap'].tables:
            cmap = table.cmap
            for unicode_value, gname in cmap.items():
                if unicode_value in swapMap:
                    cmap[unicode_value] = swapMap[unicode_value]

    def fourStyleFamily(self, position, suffix=None):
        """
        Replaces the name table and certain OS/2 values with those that will make a four-style family.
        """
        source = TTFont(fourStyleFamilySources[position])

        tf = tempfile.mkstemp()
        pathToXML = tf[1]
        source.saveXML(pathToXML, tables=['name'])
        os.close(tf[0])

        with open(pathToXML, "r") as temp:
            xml = temp.read()

        # make the changes
        if suffix:
            xml = xml.replace("Input", "Input" + suffix)

        # save the table
        with open(pathToXML, 'w') as temp:
            temp.write(xml)
            temp.write('\r')

        self.f['OS/2'].usWeightClass = source['OS/2'].usWeightClass
        self.f['OS/2'].fsType = source['OS/2'].fsType

        # write the table
        self.f['name'] = newTable('name')
        self.f.importXML(pathToXML)

    def changeNames(self, suffix=None):
        # this is a similar process to fourStyleFamily()
        tf = tempfile.mkstemp()
        pathToXML = tf[1]
        self.f.saveXML(pathToXML, tables=['name'])
        os.close(tf[0])

        with open(pathToXML, "r") as temp:
            xml = temp.read()

        # make the changes
        if suffix:
            xml = xml.replace("Input", "Input" + suffix)

        # save the table
        with open(pathToXML, 'w') as temp:
            temp.write(xml)
            temp.write('\r')

        # write the table
        self.f['name'] = newTable('name')
        self.f.importXML(pathToXML)


baseTemplatePath = os.path.split(__file__)[0]
fourStyleFamilySources = [
    os.path.join(baseTemplatePath, '_template_Regular.txt'),
    os.path.join(baseTemplatePath, '_template_Italic.txt'),
    os.path.join(baseTemplatePath, '_template_Bold.txt'),
    os.path.join(baseTemplatePath, '_template_BoldItalic.txt'),
]

fourStyleFileNameAppend = [ 'Regular', 'Italic', 'Bold', 'BoldItalic' ]


def main(args):
    four_style = False
    if args.fourStyleFamily is None:
        args.fourStyleFamily = pathlib.Path().cwd().glob('*.ttf')
        if len(list(args.fourStyleFamily)) == 0:
            print('Could not find font file in current directory', file=sys.stderr)
            exit(1)
        args.fourStyleFamily = pathlib.Path().cwd().glob('*.ttf')
    else:
        four_style = True
        four_font_style = lambda fourStyleFamily: (pathlib.Path(font_file) for font_file in fourStyleFamily if pathlib.Path(font_file).exists())
        if len(list(four_font_style(args.fourStyleFamily))) != 4:
            print('Could not find 4 font files', file=sys.stderr)
            exit(1)
        args.fourStyleFamily = four_font_style(args.fourStyleFamily)

    for i, font_style in enumerate(args.fourStyleFamily):
        print(font_style.name)
        font = TTFont(str(font_style))
        input_modifier = InputModifier(font)
        input_modifier.changeLineHeight(args.lineHeight)

        swap = {}
        for key in ['a', 'g', 'i', 'l', 'zero', 'asterisk', 'braces']:
            swap[key] = getattr(args, key, 0)

        if four_style:
            input_modifier.fourStyleFamily(i, args.suffix)
            stem = font_style.stem
            ext = font_style.suffix
            font_style_file_name = '{}_as_{}.{}'.format(stem, fourStyleFileNameAppend[i], ext.strip('.'))
            font_style = font_style.parent.joinpath(font_style_file_name)
        else:
            input_modifier.changeNames(args.suffix)
        input_modifier.swap(swap)

        if args.dest:
            dest_path = pathlib.Path(args.dest)
            if not dest_path.exists():
                dest_path.mkdir(parents=True)
            dest_path = dest_path.joinpath(font_style.name)
        else:
            dest_path = font_style

        # Take care of that weird "post" table issue, just in case. Delta#1
        try:
            del input_modifier.f['post'].mapping['Delta#1']
        except:
            pass

        input_modifier.f.save(str(dest_path))
    print('done')


def parse_argument():
    description = 'Use this script to customize one or more Input font files.'
    parser = argparse.ArgumentParser(description=description, epilog="Michel Rogers-Vallee")
    parser.add_argument('--dest', type=str, help='Directory where to put the modified files')
    parser.add_argument('--lineHeight', type=float, default=1.2, help='A multiplier for the font\'s built-in line-height.')
    parser.add_argument('--fourStyleFamily', type=str, nargs=4, help='List files to customise. If missing, it will customize all fonts in the Current Working Directory. Assigns Regular, Italic, Bold, and Bold Italic names in the order provided.')
    parser.add_argument('--suffix', type=str, default='', help='Append a suffix to the font names. Takes a string with no spaces.')
    parser.add_argument('-a', type=str, choices=['ss', 'ds'], default='ds', help='Default is the double-storey "a" (the one with the top finial). "ss" will select the single-storey "a" (the circular one)')
    parser.add_argument('-g', type=str, choices=['ss', 'ds'], default='ds', help='Default is the double-storey "g" (the one with the bottom loop). "ss" will select the single-storey "g" (the simple one)')
    parser.add_argument('-i', type=str, choices=['serif', 'serifs', 'serifs_round', 'topserif'], default='serif', help='Selects one of the alternate "i". Default is "serif" with the horizontal bar at the bottom and half bar at the top. "topserif" has the half horizontal bar at the top only. "serifs" has the half horizontal bar at the top and the bottom. "serifs_round" half horizontal bar at the top and curl at the bottom')
    parser.add_argument('-l', type=str, choices=['serif', 'serifs', 'serifs_round', 'topserif'], default='serif', help='Selects one of the alternate "l". Default is "serif" with the horizontal bar at the bottom and half bar at the top. "topserif" has the half horizontal bar at the top only. "serifs" has the half horizontal bar at the top and the bottom. "serifs_round" half horizontal bar at the top and curl at the bottom')
    parser.add_argument('--zero', type=str, choices=['slash', 'dot', 'nodot'], default='dot', help='Selects one of the alternate "0". Default is "dot".')
    parser.add_argument('--asterisk', type=str, choices=['height', 'superscript'], default='superscript', help='Select the height of the asterisk. Default is "superscript".')
    parser.add_argument('--braces', type=str, choices=['straight', 'curly'], default='curly', help='Select the braces type. Default is "curly".')

    args = parser.parse_args()

    return args

if __name__ == '__main__':
    main(parse_argument())

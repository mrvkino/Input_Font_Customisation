Update of the script given with the [Input Font](http://input.fontbureau.com/) from [Font Bureau](http://www.fontbureau.com/).
The original script his based on python 2.6 and FontTools 2.4. Also, once the font was modified on the site it was impossible to get back the original characters, only the alternate version where accessible.

For example, if "a" was changed for the single storey on the website it was impossible to get the default double storey version. This is caused by the way the script was done.

This is a version that will take any Input Font configuration and be able to re-configure the character used by the typeface.

This script uses python3.4 and the latest version of the package FontTools.

```pip3.4 install fonttools```

```
usage: inputCustomize_update.py [-h] [--dest DEST] [--lineHeight LINEHEIGHT]
                                [--fourStyleFamily FOURSTYLEFAMILY FOURSTYLEFAMILY FOURSTYLEFAMILY FOURSTYLEFAMILY]
                                [--suffix SUFFIX] [-a {ss,ds}] [-g {ss,ds}]
                                [-i {serif,serifs,serifs_round,topserif}]
                                [-l {serif,serifs,serifs_round,topserif}]
                                [--zero {slash,dot,nodot}]
                                [--asterisk {height,superscript}]
                                [--braces {straight,curly}]

Use this script to customize one or more Input font files.

optional arguments:
  -h, --help            show this help message and exit
  --dest DEST           Directory where to put the modified files
  --lineHeight LINEHEIGHT
                        A multiplier for the font's built-in line-height.
  --fourStyleFamily FOURSTYLEFAMILY FOURSTYLEFAMILY FOURSTYLEFAMILY FOURSTYLEFAMILY
                        List files to customise. If missing, it will customize
                        all fonts in the Current Working Directory. Assigns
                        Regular, Italic, Bold, and Bold Italic names in the
                        order provided.
  --suffix SUFFIX       Append a suffix to the font names. Takes a string with
                        no spaces.
  -a {ss,ds}            Default is the double-storey "a" (the one with the top
                        finial). "ss" will select the single-storey "a" (the
                        circular one)
  -g {ss,ds}            Default is the double-storey "g" (the one with the
                        bottom loop). "ss" will select the single-storey "g"
                        (the simple one)
  -i {serif,serifs,serifs_round,topserif}
                        Selects one of the alternate "i". Default is "serif"
                        with the horizontal bar at the bottom and half bar at
                        the top. "topserif" has the half horizontal bar at the
                        top only. "serifs" has the half horizontal bar at the
                        top and the bottom. "serifs_round" half horizontal bar
                        at the top and curl at the bottom
  -l {serif,serifs,serifs_round,topserif}
                        Selects one of the alternate "l". Default is "serif"
                        with the horizontal bar at the bottom and half bar at
                        the top. "topserif" has the half horizontal bar at the
                        top only. "serifs" has the half horizontal bar at the
                        top and the bottom. "serifs_round" half horizontal bar
                        at the top and curl at the bottom
  --zero {slash,dot,nodot}
                        Selects one of the alternate "0". Default is "dot".
  --asterisk {height,superscript}
                        Select the height of the asterisk. Default is
                        "superscript".
  --braces {straight,curly}
                        Select the braces type. Default is "curly".

Michel Rogers-Vallee
```